Title: Donate to Tildes
Save_as: docs/donate.html
Url: https://docs.tildes.net/donate

[TOC]

Thanks for donating to Tildes! If you have any questions, please check the answers below or contact [donate@tildes.net](mailto:donate@tildes.net).

## How to donate

**Important information for donating**

* Tildes uses third-party payment processors and does not handle or have access to any of your sensitive financial information.
* The corporation that operates Tildes is named "Spectria". Depending on the donation method you use, you may see this name on the transaction.
* Spectria is a not-for-profit corporation, but [it is *not* a charity](#why-is-tildes-a-non-profit-and-not-a-charity), so donations are not tax-deductible.

### Credit card (via Stripe)

You can donate directly using a credit card through Stripe. Note that the Stripe donation page will load third-party assets from Stripe and communicate with Stripe servers to process the transaction.

[Donate with Stripe](https://docs.tildes.net/donate-stripe)

### Patreon

You can set up a recurring monthly donation to Tildes on Patreon. There are no "patron rewards" for using Patreon and no updates will be posted there, it is simply a convenient way to manage recurring donations.

[Go to the Patreon page for Tildes](https://patreon.com/tildes)

### Cryptocurrency (via Coinbase)

You can donate Bitcoin (BTC), Bitcoin Cash (BCH), Ethereum (ETH), or Litecoin (LTC) to Tildes via Coinbase.

[Go to the Coinbase donation page for Tildes](https://commerce.coinbase.com/checkout/89160220-7474-4e61-a9fd-b8972a97f9d5)

### Interac e-Transfer

Canadians can donate to Tildes using Interac e-Transfer. Send an e-Transfer to donate@tildes.net and it will be auto-accepted (no security question necessary). Note that the recipient will be shown as "SPECTRIA".

## Information about donating

### Why should I donate to Tildes?

Tildes has no investors, no advertising, and does not sell anything (including its users' data). Donations are its only income. By donating, you're supporting a site that's chosen to avoid those other sources of revenue in order to gain the freedom to focus exclusively on acting in its users' interests.

### Who am I donating to?

Tildes is operated by Spectria, a Canadian not-for-profit corporation (corporation number 1034108-8).

### Why is Tildes a non-profit and not a charity?

Canada only grants charity status to [organizations with certain purposes](https://www.canada.ca/en/revenue-agency/services/charities-giving/giving-charity-information-donors/about-registered-charities/what-difference-between-a-registered-charity-a-non-profit-organization.html). Generally, the organization has to be devoted to relieving poverty, advancing religion or education, or the benefit of the (local, real-life) community. These are quite restricted definitions; note that [Wikimedia Canada](https://ca.wikimedia.org/wiki/About_us) (the Canadian branch of the organization behind Wikipedia) is also a non-profit and not a charity. If even building Wikipedia doesn't seem to qualify as "advancing education", I don't think there's any chance that Tildes will.

### What if you don't get enough donations to run the site full-time?

One of the best parts about avoiding venture capital and other forms of investment is that there's no pressure. Tildes doesn't have to reach certain thresholds of traffic or revenue to prevent shutting down. The worst case is just that I end up running Tildes as a side project, and hope that it eventually grows to a point where it's sustainable to work on full-time.
